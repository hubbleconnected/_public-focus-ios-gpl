#ifndef FFMPEG_DECODER_AUDIO_H
#define FFMPEG_DECODER_AUDIO_H

#include "decoder.h"

typedef void (*AudioDecodingHandler) (uint8_t*,int);


class DecoderAudio : public IDecoder
{
public:
    DecoderAudio(AVStream* stream);

    ~DecoderAudio();

    AudioDecodingHandler		onDecode;

private:

    AVCodec * pcm_codec; 
    AVCodecContext * pcm_c;
    struct SwrContext * swr;
    
    int16_t*                    mSamples;
    int                         mSamplesSize;

    bool                        prepare();
    bool                        decode(void* ptr);
    bool                        process(AVPacket *packet);
    int                         encodeToPcm_init(); 
    void                        encodeToPcm(int16_t * adpcm_buff, int len);
    int    encode_audio_with_resampling( AVCodecContext *  enc, AVCodecContext * dec,  AVFrame *decoded_frame);
};

#endif //FFMPEG_DECODER_AUDIO_H
