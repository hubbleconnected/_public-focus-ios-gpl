#!/bin/bash

set -e

#ARCHS="armv6 armv7 i386"

#arm is Main arch
ARCHS="i386 arm"

for ARCH in $ARCHS
do
  if [ -d $ARCH ]
  then
    MAIN_ARCH=$ARCH
  fi
done

if [ -z "$MAIN_ARCH" ]
then
  echo "Please compile an architecture"
  exit 1
fi


OUTPUT_DIR="dist-uarch"
rm -rf $OUTPUT_DIR

mkdir -p $OUTPUT_DIR/lib $OUTPUT_DIR/include

for LIB in $MAIN_ARCH/lib/*.a
do
  LIB=`basename $LIB`
  LIPO_CREATE=""
  for ARCH in $ARCHS
  do
    if [ -d $ARCH ]
    then
      LIPO_CREATE="$LIPO_CREATE-arch $ARCH $ARCH/lib/$LIB "
    fi
  done
  OUTPUT="$OUTPUT_DIR/lib/$LIB"
  echo "Creating: $OUTPUT"
  echo "lipo -create $LIPO_CREATE -output $OUTPUT"
  lipo -create $LIPO_CREATE -output $OUTPUT

  lipo -info $OUTPUT
done

echo "Copying headers from $MAIN_ARCH..."
cp -R $MAIN_ARCH/include/* $OUTPUT_DIR/include
