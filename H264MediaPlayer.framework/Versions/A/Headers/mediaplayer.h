#ifndef FFMPEG_MEDIAPLAYER_H
#define FFMPEG_MEDIAPLAYER_H

#include <pthread.h>
#include "Errors.h"

#include "decoder_audio.h"
#include "decoder_video.h"
#include "recorder_audio.h"
#include "recorder_video.h"

#import <UIKit/UIKit.h>




// Define all timeout values here
#define TIMEOUT_WHILE_STREAMING 20

#define FFMPEG_PLAYER_MAX_QUEUE_SIZE 48

#define FFMPEG_PLAYER_LOCAL_MIN_AUDIO_QUEUE_SIZE 4
#define FFMPEG_PLAYER_LOCAL_MIN_VIDEO_QUEUE_SIZE 8
#define FFMPEG_PLAYER_LOCAL_MAX_AUDIO_QUEUE_SIZE 8
#define FFMPEG_PLAYER_LOCAL_MAX_VIDEO_QUEUE_SIZE 16

#define FFMPEG_PLAYER_REMOTE_MIN_AUDIO_QUEUE_SIZE 32
#define FFMPEG_PLAYER_REMOTE_MIN_VIDEO_QUEUE_SIZE 32
#define FFMPEG_PLAYER_REMOTE_MAX_AUDIO_QUEUE_SIZE 40
#define FFMPEG_PLAYER_REMOTE_MAX_VIDEO_QUEUE_SIZE 40

//user for playback
// buffer less than player to handle short clips
#define FFMPEG_PLAYBACK_MIN_AUDIO_QUEUE_SIZE 30
#define FFMPEG_PLAYBACK_MIN_VIDEO_QUEUE_SIZE 30

#define CORRUPT_FRAME_TIMEOUT 3

//using namespace android;

enum media_event_type {
    MEDIA_NOP               = 0, // interface test message
    MEDIA_PREPARED          = 1,
    MEDIA_PLAYBACK_COMPLETE = 2,
    MEDIA_BUFFERING_UPDATE  = 3,
    MEDIA_SEEK_COMPLETE     = 4,
    MEDIA_SET_VIDEO_SIZE    = 5,
    MEDIA_VIDEO_STREAM_HAS_STARTED = 6,
    MEDIA_ERROR             = 100,
    MEDIA_INFO              = 200,
};

// Generic error codes for the media player framework.  Errors are fatal, the
// playback must abort.
//
// Errors are communicated back to the client using the
// MediaPlayerListener::notify method defined below.
// In this situation, 'notify' is invoked with the following:
//   'msg' is set to MEDIA_ERROR.
//   'ext1' should be a value from the enum media_error_type.
//   'ext2' contains an implementation dependant error code to provide
//          more details. Should default to 0 when not used.
//
// The codes are distributed as follow:
//   0xx: Reserved
//   1xx: Android Player errors. Something went wrong inside the MediaPlayer.
//   2xx: Media errors (e.g Codec not supported). There is a problem with the
//        media itself.
//   3xx: Runtime errors. Some extraordinary condition arose making the playback
//        impossible.
//
enum media_error_type {
    // 0xx
    MEDIA_ERROR_UNKNOWN = 1,
    // 1xx
    MEDIA_ERROR_SERVER_DIED = 100,
    MEDIA_ERROR_TIMEOUT_WHILE_STREAMING,
    // 2xx
    MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK = 200,
    // 3xx
};


// Info and warning codes for the media player framework.  These are non fatal,
// the playback is going on but there might be some user visible issues.
//
// Info and warning messages are communicated back to the client using the
// MediaPlayerListener::notify method defined below.  In this situation,
// 'notify' is invoked with the following:
//   'msg' is set to MEDIA_INFO.
//   'ext1' should be a value from the enum media_info_type.
//   'ext2' contains an implementation dependant info code to provide
//          more details. Should default to 0 when not used.
//
// The codes are distributed as follow:
//   0xx: Reserved
//   7xx: Android Player info/warning (e.g player lagging behind.)
//   8xx: Media info/warning (e.g media badly interleaved.)
//
enum media_info_type {
    // 0xx
    MEDIA_INFO_UNKNOWN = 1,
    // 7xx
    // The video is too complex for the decoder: it can't decode frames fast
    // enough. Possibly only the audio plays fine at this stage.
    MEDIA_INFO_VIDEO_TRACK_LAGGING = 700,
    // 8xx
    // Bad interleaving means that a media has been improperly interleaved or not
    // interleaved at all, e.g has all the video samples first then all the audio
    // ones. Video is playing but a lot of disk seek may be happening.
    MEDIA_INFO_BAD_INTERLEAVING = 800,
    // The media is not seekable (e.g live stream).
    MEDIA_INFO_NOT_SEEKABLE = 801,
    // New media metadata is available.
    MEDIA_INFO_METADATA_UPDATE = 802,
    
    MEDIA_INFO_FRAMERATE_VIDEO = 900,
    MEDIA_INFO_FRAMERATE_AUDIO,
    MEDIA_INFO_VIDEO_SIZE,
    MEDIA_INFO_BITRATE_BPS,
    MEDIA_INFO_HAS_FIRST_IMAGE,
    MEDIA_INFO_CORRUPT_FRAME_TIMEOUT,
    MEDIA_INFO_RECEIVED_VIDEO_FRAME,
    MEDIA_INFO_START_BUFFERING,
    MEDIA_INFO_STOP_BUFFERING,
    MEDIA_INFO_GET_AUDIO_PACKET
};



enum media_player_states {
    MEDIA_PLAYER_STATE_ERROR        = 0,
    MEDIA_PLAYER_IDLE               = 1 << 0,
    MEDIA_PLAYER_INITIALIZED        = 1 << 1,
    MEDIA_PLAYER_PREPARING          = 1 << 2,
    MEDIA_PLAYER_PREPARED           = 1 << 3,
	MEDIA_PLAYER_DECODED            = 1 << 4,
    MEDIA_PLAYER_STARTED            = 1 << 5,
    MEDIA_PLAYER_PAUSED             = 1 << 6,
    MEDIA_PLAYER_STOPPED            = 1 << 7,
    MEDIA_PLAYER_PLAYBACK_COMPLETE  = 1 << 8,
    MEDIA_PLAYER_SEEKING            = 1 << 9
};

enum interrupt_callback_id {
	INTERRUPT_CALLBACK_OFF = 0,
	INTERRUPT_CALLBACK_FIND_STREAM_INFO ,
	INTERRUPT_CALLBACK_OPEN_INPUT ,
	INTERRUPT_CALLBACK_AV_READ_FRAME
};

enum media_playback_status {
    MEDIA_PLAYBACK_STATUS_STARTED = 0,
    MEDIA_PLAYBACK_STATUS_IN_PROGRESS,
    MEDIA_PLAYBACK_STATUS_COMPLETE
};


enum media_runtime_opt
{
    MEDIA_STREAM_ALL_FRAME =0,
    MEDIA_STREAM_IFRAME_ONLY ,
    MEDIA_STREAM_RTSP_WITH_TCP,
    MEDIA_STREAM_AUDIO_MUTE,
    MEDIA_STREAM_AUDIO_NOT_MUTE,
    MEDIA_PLAYBACK_PAUSE,
    MEDIA_HAS_ENC_VIDEO,
    MEDIA_STREAM_MOVE
};


// ----------------------------------------------------------------------------
// ref-counted object for callbacks
class MediaPlayerListener
{
public:
    virtual void notify(int msg, int ext1, int ext2) = 0;
    virtual int getNextClip(char** ) =0 ;
};

class MediaPlayer
{
public:
    static MediaPlayer* Instance();
    static void  release() ;
    
    //MediaPlayer(bool forplayback);
    MediaPlayer(bool forPlayback, bool forSharedCam);
    MediaPlayer();
    void            setPlaybackAndSharedCam(bool isPlayback, bool isSharedCam);
    ~MediaPlayer();
    status_t        initFFmpegEngine();
	status_t        setDataSource(const char *url);
    
    status_t        setVideoSurface(UIImageView * _vview);
	status_t        setListener(MediaPlayerListener *listener);
	status_t        start();
	status_t        stop();
	status_t        pause();
	bool            isPlaying();
	status_t        getVideoWidth(int *w);
	status_t        getVideoHeight(int *h);
	status_t        seekTo(int64_t msec);
    status_t        seekTo(double seconds);
	status_t        getCurrentPosition(int *msec);
    int         getCurrPos();
    double          getCurrentTime();
	status_t        getDuration(int64_t *msec);
	status_t        reset();
	status_t        setAudioStreamType(int type);
	status_t		prepare();
	void            notify(int msg, int ext1, int ext2);
	status_t        suspend();
	status_t        resume();
    
    time_t			getStartTime();
	void			setStartTime(time_t start_time);
	interrupt_callback_id getInterruptCallbackId();
	void			setInterruptCallbackId(interrupt_callback_id interrupt_cb_id);
	void			setFFmpegInterrupt(bool interrupt);
    int             setPlayOption(int opts);
    MediaPlayerListener* getListener();
    
    void            videoSizeChanged();
    
    void                        sendInterrupt();
    DecoderVideo*  	mDecoderVideo;
    
    int             recordMovie(void* ptr);
    
    status_t        stopRecord();
    status_t        startRecord(const char *url_1);
    status_t         getStartPositionMovieFile(int64_t *msec);
    double          getTimeStarting();
    double          getDuration();
    bool            isShouldWait();
    void            setShouldWait(bool shouldWait);
    bool            isDisableAudioStream();
    void            setDisableAudioStream(bool isDisableAudioStream);
    void            setEncryptionKey(const char* encryption_key);
    void            setEncryptionIv(const char* encryption_iv);
    float           getLocalQueueSizeScale();
    void            setLocalQueueSizeScale(float aQueueSizeScale);
    
    void			flushAllBuffers();
    
protected:
    //MediaPlayer();
    
private:
    void						resumeDecoders();
    void						pauseDecoders();
    bool						checkStreamValid();
    AVDictionary 				**setup_find_stream_info_opts(AVFormatContext *s);
    static void                 mp_log(void* ptr, int level, const char* fmt, ...);
    static void					ffmpegNotify(void* ptr, int level, const char* fmt, va_list vl);
	static void*				startPlayer(void* ptr);
    
	static void 				decode(AVFrame* frame, double pts);
	static void 				decode(uint8_t* buffer, int buffer_size);
    static int                  should_abort_cb(void *opaque);
    
    static void *               startRecording(void* ptr); 
    
	status_t					prepareAudio();
	status_t					prepareVideo();
	bool						shouldCancel(PacketQueue* queue);
    status_t					resetDataSource(const char *new_url);
    
	void						decodeMovie(void* ptr);
    status_t                    reconfigureVideo();
    
	char*						url, *encryption_key, *encryption_iv;
	
	pthread_mutex_t             mLock;
	pthread_t					mPlayerThread;
	PacketQueue*				mVideoQueue;
    //Mutex                       mNotifyLock;
    //Condition                   mSignal;
    MediaPlayerListener*		mListener;
    AVFormatContext*			mMovieFile;
    
    bool						shouldInterrupt;
    
    int 						mAudioStreamIndex;
    int 						mVideoStreamIndex;
    DecoderAudio*				mDecoderAudio;
	//DecoderVideo*             	mDecoderVideo;
	//AVFrame*					mFrame;
	struct SwsContext*			mConvertCtx;
    
    void*                       mCookie;
    media_player_states         mCurrentState;
    int64_t                         mDuration;
    int64_t                         mStartPosition;
    int64_t                         mSeekPosition;
    double                         timeSeeking;
    bool                        mPrepareSync;
    status_t                    mPrepareStatus;
    int                         mStreamType;
    int                         mVideoWidth;
    int                         mVideoHeight;
    bool                       ffmpegEngineInitialized;
    
    time_t 						startTime;
    interrupt_callback_id 		interrupt_cb_id;
    bool                        mPlayBackOnly;
    bool                        mRtspWithTcp;
    
    bool                        suspendFlag;
    bool                        firstImageFlag;
    
    int64_t 					mGlobalStartTime;
    
    pthread_t   mRecordingThread;
    char *      record_url; 
    RecorderAudio *             mRecorderAudio;
    RecorderVideo *             mRecorderVideo;
    
    bool                        mStopRecording;

    bool						mForSharedCam;
    uint							ffmpeg_buffer_size, ffmpeg_video_buff_size, ffmpeg_audio_buff_size;
    int							max_video_buf_size, max_audio_buf_size;
    
    static void*				videoTimerTask(void* ptr);
    void						cancelVideoTimer();
    bool						mVideoTimerRunning;
    bool						firstKeyFrameDisplayed;
    bool						mReceiveDecodedFrame;
    bool						isLoadingDialogShowing;
    pthread_t					mVideoTimerThread;
    int64_t                     timeStartPause;
    int64_t                     timeTotalPause;
    int64_t                     timeDeltaSeek;
    int64_t                     startPositionMovieFile;
    double                      timePts;
    double                      timeStarting;
    double                      playbackDuration;
    static MediaPlayer*         _instance;
    AVPicture                   pictureTmp;
    bool                        shouldWait;
    bool                        playSecureStream;
    bool                        disableAudioStream;
    float                       localQueueSizeScale;
};

#endif // FFMPEG_MEDIAPLAYER_H
