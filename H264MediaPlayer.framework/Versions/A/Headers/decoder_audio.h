#ifndef FFMPEG_DECODER_AUDIO_H
#define FFMPEG_DECODER_AUDIO_H

#include "decoder.h"
#include "recorder_audio.h"

typedef void (*AudioDecodingHandler) (uint8_t*,int);

class DecoderVideo;

class DecoderAudio : public IDecoder
{
public:
    DecoderAudio(AVStream* stream);

    ~DecoderAudio();

    AudioDecodingHandler		onDecode;
    
    /* need to pass the current Audio buffer size */ 
    double                      get_audio_clock();
  

    void setRecorderAudio(RecorderAudio  *rec);
    void encodeToAdpcm_swf(uint8_t * buffer, int len);
    int   encodeToAdpcm_init();
    void  passBuffer(uint8_t * buffer, int len);
    void resume();
    
private:

      bool                        needRealTimeAudio;
    
    AVCodec * pcm_codec; 
    AVCodecContext * pcm_c;
    struct SwrContext * swr;
    RecorderAudio   * mRecorder;

    uint8_t * leftOverBuff;
    int       leftOverLen;
    AVCodec * adpcm_codec;
    AVCodecContext * adpcm_c;
    
    double                      audio_clock; //maintain the latest pts
    
    int16_t*                    mSamples;
    int                         mSamplesSize;

    bool                        prepare();
    bool                        decode(void* ptr);
    bool                        process(AVPacket *packet);
    int                         encodeToPcm_init(); 
    void                        encodeToPcm(int16_t * adpcm_buff, int len);
    int    encode_audio_with_resampling( AVCodecContext *  enc, AVCodecContext * dec,  AVFrame *decoded_frame);
    int     synchronize_audio( AVCodecContext * codec_context, uint8_t *samples, int samples_size );
    
    double audio_diff_cum, audio_diff_avg_coef,audio_diff_avg_count,audio_diff_threshold;
    int64_t                     start_time;
   
};

#endif //FFMPEG_DECODER_AUDIO_H
