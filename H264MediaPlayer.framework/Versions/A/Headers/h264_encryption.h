/*******************************************************************************
* Nxcomm 2013
* Product: Cmdserver library
* @Author: trungdo@
* @DateTime: 12Dec2013 - 9:30:00@
* @Version:  0.1@
* Description: H264 encryption header
*******************************************************************************/

#ifndef __H264_AES_H__
#define __H264_AES_H__

#include <stdint.h>

int h264_aes_init(struct aes_context *ctx, uint8_t *key, uint8_t nbytes);
void h264_aes_encrypt(struct aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length);
void h264_aes_decrypt(struct aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length);

#endif  // end of __H264_AES_H__

// @log:h264_encryption.h@
// DateTime: 12Dec2013 - 09:30:00
// Author: trungdo
// Description: Initial version
