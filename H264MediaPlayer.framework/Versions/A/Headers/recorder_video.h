#ifndef FFMPEG_RECORDER_VIDEO_H
#define FFMPEG_RECORDER_VIDEO_H

#include "decoder.h"

class RecorderVideo : public IDecoder
{
public:
    RecorderVideo(AVStream* stream);
    
    ~RecorderVideo();
    
    void     write_video_frame(AVFormatContext *oc);
   
    bool     prepare( AVFormatContext *oc,   AVStream  * src_context );
   // bool     prepare( AVFormatContext *oc,  AVCodecContext * src_context );

    
    AVStream * getVideoStream();
    void     cleanUp();
    bool     hasUnencodedFrames();
private:
    
    AVStream *       src_stream;
    AVCodecContext * src_codec_context ;
    bool                       mStopRecording;
    bool first_time ;
    int64_t first_pts, first_dts;
    
    
    
    void  stopRecording();
    AVStream *    add_stream(AVFormatContext *oc, AVCodec **codec,
                             enum AVCodecID codec_id);
    
};

#endif
