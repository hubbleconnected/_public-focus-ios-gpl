#ifndef FFMPEG_DECODER_VIDEO_H
#define FFMPEG_DECODER_VIDEO_H

#include "decoder.h"
#include "decoder_audio.h"
extern "C" {

 
    /* borrow from mpegvideo.h */
    /**
     * Motion estimation context.
     */
    typedef struct MotionEstContext{
        AVCodecContext *avctx;
        int skip;                          ///< set if ME is skipped for the current MB
        int co_located_mv[4][2];           ///< mv from last P-frame for direct mode ME
        int direct_basis_mv[4][2];
        uint8_t *scratchpad;               ///< data area for the ME algo, so that the ME does not need to malloc/free
        uint8_t *best_mb;
        uint8_t *temp_mb[2];
        uint8_t *temp;
        int best_bits;
        uint32_t *map;                     ///< map to avoid duplicate evaluations
        uint32_t *score_map;               ///< map to store the scores
        unsigned map_generation;
        int pre_penalty_factor;
        int penalty_factor;                /**< an estimate of the bits required to
                                            code a given mv value, e.g. (1,0) takes
                                            more bits than (0,0). We have to
                                            estimate whether any reduction in
                                            residual is worth the extra bits. */
        int sub_penalty_factor;
        int mb_penalty_factor;
        int flags;
        int sub_flags;
        int mb_flags;
        int pre_pass;                      ///< = 1 for the pre pass
        int dia_size;
        int xmin;
        int xmax;
        int ymin;
        int ymax;
        int pred_x;
        int pred_y;
        uint8_t *src[4][4];
        uint8_t *ref[4][4];
        int stride;
        int uvstride;
        /* temp variables for picture complexity calculation */
        int mc_mb_var_sum_temp;
        int mb_var_sum_temp;
        int scene_change_score;
        /*    cmp, chroma_cmp;*/
        void *hpel_put[4];
        void *hpel_avg[4];
        void * qpel_put[16];
        void * qpel_avg[16];
        void * mv_penalty[4096*2+1];  ///< amount of bits needed to encode a MV
        uint8_t *current_mv_penalty;
        int (*sub_motion_search)(struct MpegEncContext * s,
                                 int *mx_ptr, int *my_ptr, int dmin,
                                 int src_index, int ref_index,
                                 int size, int h);
    }MotionEstContext;

    
      /* borrow from parser.h */
    typedef struct ParseContext{
        uint8_t *buffer;
        int index;
        int last_index;
        unsigned int buffer_size;
        uint32_t state;             ///< contains the last few bytes in MSB order
        int frame_start_found;
        int overread;               ///< the number of bytes which where irreversibly read from the next frame
        int overread_index;         ///< the index into ParseContext.buffer of the overread bytes
        uint64_t state64;           ///< contains the last 8 bytes in MSB order
    } ParseContext;

    
    /* borrow from getbits.h */
    typedef struct GetBitContext {
        const uint8_t *buffer, *buffer_end;
        int index;
        int size_in_bits;
        int size_in_bits_plus8;
    } GetBitContext;
    
    
    /* borrow from error_resilience.h */
    typedef struct ERContext_{
        AVCodecContext *avctx;
        void  *dsp; //DSPContext *
        
        int *mb_index2xy;
        int mb_num;
        int mb_width, mb_height;
        int mb_stride;
        int b8_stride;
        
        int error_count, error_occurred;
        uint8_t *error_status_table;
        uint8_t *er_temp_buffer;
        int16_t *dc_val[3];
        uint8_t *mbskip_table;
        uint8_t *mbintra_table;
        int mv[2][4][2];
        
        struct Picture *cur_pic;
        struct Picture *last_pic;
        struct Picture *next_pic;
        
        uint16_t pp_time;
        uint16_t pb_time;
        int quarter_sample;
        int partitioned_frame;
        int ref_count;
        
        void (*decode_mb)(void *opaque, int ref, int mv_dir, int mv_type,
                          int (*mv)[2][4][2],
                          int mb_x, int mb_y, int mb_intra, int mb_skipped);
        void *opaque;
    } ERContext_;
    
    typedef struct H264Context {
        AVCodecContext *avctx;
        char vdsp[8]; //VideoDSPContext vdsp;
        char h264dsp[132]; //H264DSPContext h264dsp;
        char h264chroma[32]; //H264ChromaContext h264chroma;
        char h264qpel[512]; //H264QpelContext h264qpel;
        char me[344]; //MotionEstContext me;
        char parse_context[40]; //ParseContext parse_context;
        char gb[20]; //GetBitContext gb;
        char dsp[1256]; //DSPContext dsp;
        ERContext_ er;
    } H264Context;
    
} // end of extern C


#define FFMPEG_FILTER_CORRUPTED_FRAME 1

//./ffplay.c:75:
#define AV_SYNC_THRESHOLD_MIN 0.01
#define AV_SYNC_THRESHOLD_MAX 0.1
#define AV_NOSYNC_THRESHOLD 5.0

#define  USE_DROP_FRAME_WHEN_ERROR 1

typedef void (*VideoDecodingHandler) (AVFrame*,double);

class DecoderVideo : public IDecoder
{
public:
    DecoderVideo(AVStream* stream);
    ~DecoderVideo();

    DecoderAudio *              audioDecoder;
    VideoDecodingHandler		onDecode;
    void                        setPlayIFrameOnly(bool);
    //void                        stop();
    double                      get_video_clock();
    bool                        needRealTimeVideo;
    void						resume();

private:
	AVFrame*					mFrame;
	double						mVideoClock;
    int                        	mShowingIFrame;
    bool						mSkipUntilNextKeyFrame;
    bool						should_waiting;

 
    bool                        prepare();
    double 						synchronize(AVFrame *src_frame, double pts);
    bool                        decode(void* ptr);
    bool                        process(AVPacket *packet);
    static int					getBuffer(struct AVCodecContext *c, AVFrame *pic);
    static int					getBuffer2(struct AVCodecContext *s, AVFrame *frame, int flags);
    static void					releaseBuffer(struct AVCodecContext *c, AVFrame *pic);
    
       double                      cal_delay(double pts);
    
    double frame_last_pts;
    double frame_last_delay;
    double frame_timer;
    
    
    //double          video_clock; ///<pts of last decoded frame / predicted pts of next decoded frame
    double          video_current_pts; ///<current displayed pts (different from video_clock if frame fifos are used)
    int64_t         video_current_pts_time;  ///<time (av_gettime) at which we updated video_current_pts - used to have running video pts
    
};

#endif //FFMPEG_DECODER_AUDIO_H
