#ifndef FFMPEG_RECORDER_AUDIO_H
#define FFMPEG_RECORDER_AUDIO_H

#include "decoder.h"
#define FF_BUFQUEUE_SIZE  64

extern "C" {
#include "libavfilter/bufferqueue.h"
#include "libavfilter/buffersrc.h"
#include "libavfilter/buffersink.h"
#include "libavutil/mathematics.h"
#include "libswresample/swresample.h"
#include "libavutil/opt.h"
#include "libavutil/time.h"
#include "libavutil/samplefmt.h"
}

class RecorderAudio : public IDecoder
{
public:
    RecorderAudio(AVStream* stream);
    
    ~RecorderAudio();
    
    
    
    void     write_audio_frame2(AVFormatContext *oc);
    void     write_audio_frame4(AVFormatContext *oc);
    bool     prepare( AVFormatContext *oc,  AVCodecContext * src_context );
    
    
    
    AVStream * getAudioStream();
    void     cleanUp();
    void     addFrame (AVFrame * frame);
    
    void     initBuffQueue();
    void     destroyBuffQueue();
    bool     hasUnencodedFrames();
    double   nextPts();
    void     flush_all_audio(); 
private:
    
    pthread_mutex_t     mLock;
    pthread_cond_t      mCondition;
    
    
    AVCodecContext * src_codec_context ;
    
    AVCodec *adpcm_codec; //use for recording
    AVCodecContext *adpcm_c; //use for recording
    AVFilterContext *buffersink_ctx;
    AVFilterContext *buffersrc_ctx;
    AVFilterGraph *filter_graph;
    struct SwrContext * swr2;
    
    double next_pts;
    
    int16_t*   mRecordPacket;
    int       mSamplesSize;
    
    uint8_t * leftOverBuff;
    int       leftOverLen;
    
    int64_t recordStartTime;
    
    struct FFBufQueue * audioFrameQueue;
    bool                       mStopRecording;
    
    int init_filters(const char *filters_descr);
    int							encodeToAdpcm_init(AVFormatContext *oc);
    
    //        int resample_audio_for_record(AVCodecContext *  enc, AVCodecContext * dec,
    //                AVFrame *decoded_frame,   int*  out_len);
    
    int resample_audio_for_record(AVCodecContext *  enc, AVCodecContext * dec,
                                  AVFrame *decoded_frame,   int*  out_len , uint8_t * * out_buf);
    
    
    
    int   write_audio_to_stream(AVFormatContext *oc,  uint8_t * buffer, int  len);
    
    void stopRecording();
    AVStream *    add_stream(AVFormatContext *oc, AVCodec **codec,
                             enum AVCodecID codec_id);
    
    AVFrame * getFrame();
};

#endif
