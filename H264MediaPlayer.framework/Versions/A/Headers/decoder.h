#ifndef FFMPEG_DECODER_H
#define FFMPEG_DECODER_H


#include <assert.h>
#include "thread.h"
#include "packetqueue.h"

enum {
    AV_SYNC_AUDIO_MASTER,
    AV_SYNC_VIDEO_MASTER,
    AV_SYNC_EXTERNAL_MASTER,
};





class IDecoder : public Thread
{
public:
	IDecoder(AVStream* stream );
	~IDecoder();
	
    void						stop();
	void						enqueue(AVPacket* packet);
    void						dequeue();
    void						flush();
	int							packets();
    void                        setLog( void (* log_callback)      (void* ptr, int level, const char* fmt, ...));
    int                         clock_sync;
    int64_t                     external_clock_time;

    bool						forPlayback;
	void						setPlaybackMode(bool isForPlayback);

    
    bool						isRecording;
	void						setRecording(bool);
    void						pause();
	bool						isPaused();
    PacketQueue*                mQueue;
    void						setSyncMaster(int syncType);
    
protected:
    
    
    //PacketQueue*                mQueue;
    AVStream*             		mStream;
    bool						isPause;

    virtual bool                prepare();
    virtual bool                decode(void* ptr);
    virtual bool                process(AVPacket *packet);
	void						handleRun(void* ptr);
    void (* idecoder_log)      (void* ptr, int level, const char* fmt, ...);
    virtual void				resume();

};

#endif //FFMPEG_DECODER_H
